.DEFAULT: help
.PHONY: help
help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

setup: ## Install dependencies
	$(info Running setup)

unit_tests: ## Run unit tests
	$(info Running unit tests)

package: clean setup ## Prepare project for deployment
	$(info Packaging project)

clean: ## Delete build artifacts
	$(info Cleaning project)
