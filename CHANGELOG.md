# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.5.1] - 2022-02-28

- Added MIT License to repo
- Made repo public

## [0.5.0] - 2021-12-17

- Update to latest version of Hugo
